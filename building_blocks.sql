

CREATE TABLE IF NOT EXISTS `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


--
-- Table structure for table `product_product`
--

CREATE TABLE IF NOT EXISTS `product_product` (
  `prodid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` longtext NOT NULL,
  `background_image` longtext NOT NULL,
  `logo` longtext NOT NULL,
  `product_type` varchar(50) NOT NULL,
  PRIMARY KEY (`prodid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `product_product`
--

INSERT INTO `product_product` (`prodid`, `name`, `title`, `description`, `background_image`, `logo`, `product_type`) VALUES
(1, 'Airbnb', 'Bed & Breakfast', 'Earn $ for every new customer', 'https://a0.muscache.com/im/pictures/c3ea4623-9f14-44d8-9426-ef58d3bd8acf.jpg?aki_policy=xx_large', 'https://a0.muscache.com/airbnb/static/logos/belo-200x200-4d851c5b28f61931bf1df28dd15e60ef.png', 'Opportunities'),
(2, 'Winc', 'Wines that delight you', 'Wines that delight you', 'https://a0.muscache.com/im/pictures/255bc11b-e3d8-44cc-973f-255ed6b34ae7.jpg?aki_policy=xx_large', 'https://a0.muscache.com/airbnb/static/logos/belo-200x200-4d851c5b28f61931bf1df28dd15e60ef.png', 'Featured'),
(3, 'SAUCEY', '30-minutes alcohol delivery', 'desc for 30-minutes alcohol delivery', 'https://a0.muscache.com/im/pictures/12426057/2292b61c_original.jpg?aki_policy=xx_large', 'https://a0.muscache.com/airbnb/static/logos/belo-200x200-4d851c5b28f61931bf1df28dd15e60ef.png', 'Opportunities');

-- --------------------------------------------------------


CREATE TABLE IF NOT EXISTS `user_user` (
  `userid` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `address_verified` tinyint(1) NOT NULL,
  `email` varchar(100) NOT NULL,
  `email_verified` tinyint(1) NOT NULL,
  `fullname` varchar(50) NOT NULL,
  `mobilenumber` varchar(50) NOT NULL,
  `otp_verified` tinyint(1) NOT NULL,
  PRIMARY KEY (`userid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `user_user`
--

INSERT INTO `user_user` (`userid`, `username`, `password`, `address_verified`, `email`, `email_verified`, `fullname`, `mobilenumber`, `otp_verified`) VALUES
(1, 'admin', 'admin', 1, 'm@a.in', 0, 'Admin', '', 0);

