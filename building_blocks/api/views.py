from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from user.models import User
from product.models import Product

@csrf_exempt
def login(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')
    data = {'isSuccess':False, 'Data':[]}
    if username and password:
        user_obj = User.objects.filter(username=username,password=password)                
        if len(user_obj) > 0:
            data = {'isSuccess':True,'Data':{'user_id':user_obj[0].userid},'Message':'Loggedin Successfully'}
        else:
            data['Message'] = "Incorrect username or password"
    else:
        data['Message'] = "Empty username or password"
    return JsonResponse(data)

@csrf_exempt    
def dashboard(request):
    userid = request.POST.get('userid','')
    data = {'isSuccess':False, 'Data':[]}
    if userid:
        feature = Product.objects.filter(product_type='Featured')
        feature_data = []
        if len(feature) > 0:
            feature_data = {'id':feature[0].prodid, 'name':feature[0].name, 'title':feature[0].title, 'description':feature[0].description, 'background_image':feature[0].background_image, 'logo':feature[0].logo,'product_type':feature[0].product_type}
            
        opportunities_data = []
        opportunities = Product.objects.filter(product_type='Opportunities')
        if len(opportunities) > 0:
            for product in opportunities:                
                prod_data = {'id':product.prodid, 'name':product.name, 'title':product.title, 'description':product.description, 'background_image':product.background_image, 'logo':product.logo,'product_type':product.product_type}
                opportunities_data.append(prod_data)
                
        user_obj = User.objects.get(userid=userid)
        profile_complete = 0
        profile_complete_percentage = 0
        total_profile_complete_count = 5
        if user_obj.email:
            profile_complete += 1
        if user_obj.mobilenumber:
            profile_complete += 1
        if user_obj.email_verified:
            profile_complete += 1
        if user_obj.otp_verified:
            profile_complete += 1
        if user_obj.address_verified:
            profile_complete += 1
        if profile_complete > 0:
            profile_complete_percentage = (profile_complete*100)/total_profile_complete_count
        data = {'isSuccess':True, 'Data':{'feature_data':feature_data, 'opportunities_data' : opportunities_data, 'profile_complete_percentage' : profile_complete_percentage},'Message':"Dashboard Details"}
    else:
        data['Message'] = "Userid not found"
    print(data)
    return JsonResponse(data)
        