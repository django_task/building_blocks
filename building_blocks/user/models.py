from django.db import models

class User(models.Model):
    userid = models.AutoField(primary_key=True)
    username = models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    fullname = models.CharField(max_length=50)
    email = models.EmailField(max_length=100)
    mobilenumber = models.CharField(max_length=50)
    email_verified = models.BooleanField(default=False)
    otp_verified = models.BooleanField(default=False)
    address_verified = models.BooleanField(default=False)