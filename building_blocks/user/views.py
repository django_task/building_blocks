from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponseRedirect
from user.models import User
from product.models import Product

@csrf_exempt
def login(request):
    username = request.POST.get('username','')
    password = request.POST.get('password','')    
    if username and password:
        user_obj = User.objects.filter(username=username,password=password)        
        print(len(user_obj))
        if len(user_obj) > 0:
            if 'user_id' not in request.session:
                request.session['user_id'] = user_obj[0].userid
                
            if 'full_name' not in request.session and user_obj[0].fullname:
                request.session['full_name'] = user_obj[0].fullname
                
            return HttpResponseRedirect("dashboard")
    return HttpResponseRedirect("/?msg=Invalid username or password")
        
def dashboard(request):
    if 'user_id' in request.session:
        user_id = request.session['user_id']
        feature = Product.objects.filter(product_type='Featured')
        opportunities = Product.objects.filter(product_type='Opportunities')
        user_obj = User.objects.get(userid=user_id)
        profile_complete = 0
        profile_complete_percentage = 0
        total_profile_complete_count = 5
        if user_obj.email:
            profile_complete += 1
        if user_obj.mobilenumber:
            profile_complete += 1
        if user_obj.email_verified:
            profile_complete += 1
        if user_obj.otp_verified:
            profile_complete += 1
        if user_obj.address_verified:
            profile_complete += 1
        profile_complete_percentage = (profile_complete*100)/total_profile_complete_count
        return render(request,"dashboard.html",context={'opportunities':opportunities, 'feature' : feature,'profile_complete_percentage':profile_complete_percentage})
    else:
        return HttpResponseRedirect("/")

def logout(request):
    if 'user_id' in request.session:
        del request.session['user_id']
                
    if 'full_name' in request.session:
        del request.session['full_name']
    return HttpResponseRedirect("/")
            
    