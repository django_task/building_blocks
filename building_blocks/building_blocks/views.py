from django.shortcuts import render
from django.http import HttpResponseRedirect

def index(request):
    if 'user_id' in request.session:
        return HttpResponseRedirect("user/dashboard")
    msg = request.GET.get('msg','')
    return render(request,"index.html",context={'msg':msg})