# Generated by Django 2.1 on 2018-08-02 08:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('product', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='product',
            name='product_type',
            field=models.CharField(default=None, max_length=50),
            preserve_default=False,
        ),
    ]
