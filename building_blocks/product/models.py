from django.db import models

# Create your models here.
class Product(models.Model):
    prodid = models.AutoField(primary_key=True)
    name = models.CharField(max_length=50)
    title = models.CharField(max_length=50)
    description = models.TextField()
    background_image = models.TextField()
    logo = models.TextField()
    product_type = models.CharField(max_length=50)
